
module top;
	
	
	typedef enum bit[2:0] {and_op  = 3'b000,
                             or_op = 3'b001,                   
                            add_op = 3'b100,
                            sub_op = 3'b101,
                            wrong_op = 3'b111,
                            wrong_op1 = 3'b110,
                            wrong_op2 = 3'b011,
                            wrong_op3 = 3'b010} operation_t;
	
	typedef enum bit[2:0] {and_op_sin  = 3'b000,
                             or_op_sin = 3'b001,                   
                            add_op_sin = 3'b100,
                            sub_op_sin = 3'b101,
                            wrong_op_sin = 3'b111,
                            wrong_op1_sin = 3'b110,
                            wrong_op2_sin = 3'b011,
                            wrong_op3_sin = 3'b010} operation_t_sin;
 
    bit clk; 	  // posedge active
	bit rst_n; 	  // active low
	bit sin;	  // serial data input
	bit sout;	  // serial data output
	operation_t  op;
	
	int predicted_result;
	bit[31:0] predicted_bit;
	int DONE1;
	int DONE2;
	
	bit [31:0] A;
	bit [31:0] B;
	
	bit [31:0] A_sin;
	bit [31:0] B_sin;
	bit [7:0] CTL_sin;
	operation_t_sin op_sin;
	bit [3:0] crc_sin;
	
	
	bit [98:0] bufor;
	bit [54:0] bufor_sout;
	bit [31:0] C;
	bit [3:0] flags;
    bit [3:0] crc_out;
	
	
	bit parity;
	bit [3:0] crc;
	bit [7:0] payload;
	
	bit [3:0] error_flag;
	bit error_crc;
	bit error_op_set;
	bit error_data;
	
	
		
//------------------------------------------------------------------------------
// ALU instantiation
//------------------------------------------------------------------------------

   mtm_Alu DUT (.clk(clk), .rst_n(rst_n), .sout(sout), .sin(sin));
 
 // CRC calc
 
 function bit  [3:0] calc_crc(
	 input bit [31:0] B,
	 input bit [31:0] A,
	 input bit [2:0] op
	 
	 
	 );

		 
    reg [67:0] d;
    reg [3:0] c;
    reg [3:0] newcrc;
	 
  begin
    d = { B, A, 1'b1, op};
    c = 4'b0000;

    newcrc[0] = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
    newcrc[1] = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
    newcrc[2] = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
    newcrc[3] = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];
	  
    return newcrc;
  end
 endfunction : calc_crc
 
 
 //---------------------------------
// Random data generation functions

function operation_t get_op();
      bit [2:0] op_choice;
      op_choice = $random;
      case (op_choice)
        3'b000 : return and_op;
        3'b001 : return or_op;
        3'b100 : return add_op;
        3'b101 : return sub_op;
	    3'b111 : return wrong_op;
	    3'b110 : return wrong_op1;
	    3'b010 : return wrong_op3;
	    3'b011 : return wrong_op2;
	   // default: return get_op();
      endcase 
   endfunction : get_op
 
 //---------------------------------
   function bit [31:0] get_data();
	   bit [2:0] zero_ones;
	   zero_ones = $random;
   if (zero_ones == 3'b000)
        return 32'h0000_0000;
      else if (zero_ones == 3'b111)
        return 32'hFFFF_FFFF;
      else
        return $random;
   endfunction : get_data
//---------------------------------

// TASK TASK TASK TASK TASK TASK TASK TASK TASK TASK TASK TASK
 

  task DATA( input bit [7:0] payload);
	  begin : DATA_main
		 @(posedge clk);
		 sin <= 1'b0;
		  
		 @(posedge clk);
		 sin <= 1'b0;
		  
		 
		  for(int i=7 ; i >= 0; i--)
		   begin
			@(posedge clk);  
			sin <= payload[i];	  
		   end
		  
		 @(posedge clk);
		  sin <= 1'b1;
	  end
  endtask 
  
  task CTL( input bit [2:0] op, input bit [3:0] crc );
	  begin : CTL_main
		  
		 @(posedge clk);
		 sin <= 1'b0;
		  
		 @(posedge clk);
		 sin <= 1'b1;
		  
		 @(posedge clk);
		 sin <= 1'b0;
		  
		 
		  for(int i=2 ; i >= 0; i--)
		   begin
			@(posedge clk);  
			sin <= op[i];	  
		   end
		   
		  for(int i=3 ; i >= 0; i--)
		   begin
			@(posedge clk);   
			sin <= crc[i];	  
		   end 
		  
		 @(posedge clk);
		  sin <= 1'b1;
		  
	  end
  endtask 

// TASK TASK TASK TASK TASK TASK TASK TASK TASK TASK TASK TASK
//------------------------------------------------------------------------------
// Coverage block
//-----------------------------------------------------------------------------
covergroup op_cov;
	 
    option.name = "cg_op_cov";

      coverpoint op {
         // #A1 test all operations
         bins A1_single_operation[] = {[add_op : sub_op]};
         

         // #A2 test all operations after reset
        bins A2_rst_opn[] = (rst_n => [add_op:sub_op]);

         // #A3 test reset after all operations
        bins A3_opn_rst[] = ([add_op:sub_op] => rst_n);

         // #A4 two operations in row
         bins A4_twoops[] = ([add_op:sub_op] [* 2]);

         // #A5 error  CRC
        bins A5_error_crc[] = {error_crc};
	      
	      // #A6 error data
         bins A6_error_data[] = {error_data};
	      
	      // #A7 error op_set
         bins A7_op_set[] = {error_op_set};
	      
	      // #A8 reset after all errors
        bins A8_error_rst[] = ([error_crc:error_data] => rst_n);
	      
	      // #A9 reset after all errors
         bins A9_rst_error[] = (rst_n =>[error_crc:error_data]);
      }
	 
	 
 endgroup
 
 covergroup zeros_or_ones_on_ops;
	 
	 
	option.name = "cg_zeros_or_ones_on_ops";

      all_ops : coverpoint op {
         
      }

      a_leg: coverpoint A {
         bins zeros = {'h00000000};
         bins others= {['h00000001:'hFFFFFFFE]};
         bins ones  = {'hFFFFFFFF};
      }

      b_leg: coverpoint B {
         bins zeros = {'h00000000};
         bins others= {['h000000001:'hFFFFFFFE]};
         bins ones  = {'hFFFFFFFF};
      }

      B_op_00_FF:  cross a_leg, b_leg, all_ops {

         // #B1 simulate all zero input for all the operations

         bins B1_add_ALL_00 = binsof (all_ops) intersect {add_op} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         bins B1_and_ALL_00 = binsof (all_ops) intersect {and_op} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         bins B1_sub_ALL_00 = binsof (all_ops) intersect {sub_op} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         bins B1_or_ALL_00 = binsof (all_ops) intersect {or_op} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         // #B2 simulate all one input for all the operations

         bins B2_add_ALL_FF = binsof (all_ops) intersect {add_op} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));

         bins B2_and_ALL_FF = binsof (all_ops) intersect {and_op} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));

         bins B2_sub_ALL_FF = binsof (all_ops) intersect {sub_op} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));

         bins B2_or_ALL_FF = binsof (all_ops) intersect {or_op} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));


         ignore_bins others_only =
                                  binsof(a_leg.others) && binsof(b_leg.others);

      } 
	 
 endgroup
 
  op_cov oc;
  zeros_or_ones_on_ops c_00_FF;
 
 initial begin : coverage
   
      oc = new();
      c_00_FF = new();
  
      forever begin : sample_cov
         @(negedge clk);
         oc.sample();
         c_00_FF.sample();
     end
   end : coverage
 
 
 //------------------------------------------------------------------------------
// END COVERAGE BLOCK
//------------------------------------------------------------------------------ 
 
 
 //------------------------------------------------------------------------------
// Clock generator
//------------------------------------------------------------------------------

   initial begin : clk_gen
      clk = 0;
      forever begin : clk_frv
         #10;
         clk = ~clk;
      end
   end

//------------------------------------------------------------------------------
// Funkcja
//------------------------------------------------------------------------------
 task write( input bit [31:0] B , input bit [31:0] A , input bit [2:0] op, input bit [3:0] crc);
	 begin : write_main
	DATA( B[31:24] );
 	DATA( B[23:16] );
 	DATA( B[15:8] );
 	DATA( B[7:0] );
 	DATA( A[31:24] );
 	DATA( A[23:16] );
 	DATA( A[15:8] );
 	DATA( A[7:0] );
 	CTL(  op, crc );
		 
	 end
 endtask


//------------------------------------------------------------------------------
// Tester
//------------------------------------------------------------------------------
 
 
 
 
 
 
 initial begin : tester	 


	 rst_n = 1'b0;
	 sin = 1'b1;
	 @(negedge clk);
     @(negedge clk);
     rst_n = 1'b1;
	 @(negedge clk);
     @(negedge clk);
	 
	 repeat (100) begin : tester_main
		 B = get_data();
		 A = get_data();
		 op = get_op();
		 crc = calc_crc( B , A , op);
		 
		 write( B, A, op, crc);
	 end
	 $finish;
	 
 end : tester
 
 
 
 
//------------------------------------------------------------------------------
// Scoreboard
//--

function [2:0] calc_crc_out (input bit [31:0] C, input bit [3:0] flags);

    
    reg [36:0] d;
    reg [2:0] c;
    reg [2:0] newcrc;
  begin
    d = {C, 1'b0, flags};
    c = 3'b000;

    newcrc[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1];
    newcrc[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
    newcrc[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
    calc_crc_out = newcrc;
  end
  endfunction




bit    queue_1[$:99];
bit    queue_2[$:44];

initial begin : read_sout
	repeat (98) begin : read_main
		
		
@(negedge sout) begin
	for(int i=54 ; i>= 0; i--)
		   begin
		 
			@(posedge clk)
			bufor_sout[i] <= sout;		  
		   end 	
	end

// POPRAWNY SOUT DANA C NA WYJSCIU + FLAGI ITP
	if (bufor_sout[54] == 1'b0 & bufor_sout[53] == 1'b0 )begin 
		for(int i=0 ; i<= 7; i++)
		begin			
			C[31-i] <= bufor_sout[52-i];
			
		end
		
		for(int i=0 ; i<= 7; i++)
		begin			
			C[23-i] <= bufor_sout[41-i];
			
	    end	
		for(int i=0 ; i<= 7; i++)
		begin			
			C[15-i] <= bufor_sout[30-i];
			
	    end		
		for(int i=0 ; i<= 7; i++)
		begin			
			C[7-i] <= bufor_sout[19-i];
			
		end
		for(int i=0 ; i<= 3; i++)
		begin			
			flags[3-i] <= bufor_sout[8-i];
			
		end
		for(int i=0 ; i<= 2; i++)
		begin			
			crc_out[2-i] <= bufor_sout[4-i];
			
	    end	
	end
	
	
	
	
	
	// TYLKO BDNY CTR
 if (bufor_sout[54] == 1'b0 & bufor_sout[53] == 1'b1 & bufor_sout[52] == 1'b1 )
	 begin
		 
     
		error_crc = bufor_sout[50];
	    error_op_set =  bufor_sout[49] ;
	    error_data = bufor_sout[51];		
		parity = bufor_sout[45];
    
     end 





end
end : read_sout

/////////////////////////////////////////////////////////////
//Przewidywanie wyniku
////////////////////////////////////////////////////////////
  initial begin : read_sin
repeat (98) begin : read_sin_main
	

@(negedge sin) begin
	 for(int i=98 ; i>= 0; i--)
		   begin
		 
			@(posedge clk)
			bufor[i] <= sin;
		  
		   end 	  
   end
		
if(bufor[98] == 1'b0 & bufor[97] == 1'b0 & bufor[88] == 1'b1)begin
//CALC BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB		   
for(int i=0 ; i<= 7; i++)
		begin			
			B_sin[31-i] <= bufor[96-i];
			
		end
end

if(bufor[87] == 1'b0 & bufor[86] == 1'b0 & bufor[77] == 1'b1)begin
for(int i=0 ; i<= 7; i++)
		begin			
			B_sin[23-i] <= bufor[85-i];
			
		end	
end	

if(bufor[76] == 1'b0 & bufor[75] == 1'b0 & bufor[66] == 1'b1)begin		
for(int i=0 ; i<= 7; i++)
		begin			
			B_sin[15-i] <= bufor[74-i];
			
		end		
end		
if(bufor[65] == 1'b0 & bufor[64] == 1'b0 & bufor[55] == 1'b1)begin		
for(int i=0 ; i<= 7; i++)
		begin			
			B_sin[7-i] <= bufor[63-i];
			
	    end		
end
//CALC AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA


if(bufor[54] == 1'b0 & bufor[53] == 1'b0 & bufor[44] == 1'b1)begin		
for(int i=0 ; i<= 7; i++)
		begin			
			A_sin[31-i] <= bufor[52-i];
			
		end
end		
if(bufor[43] == 1'b0 & bufor[42] == 1'b0 & bufor[33] == 1'b1)begin				
for(int i=0 ; i<= 7; i++)
		begin			
			A_sin[23-i] <= bufor[41-i];
			
		end	
end		
if(bufor[32] == 1'b0 & bufor[31] == 1'b0 & bufor[22] == 1'b1)begin				
for(int i=0 ; i<= 7; i++)
		begin			
			A_sin[15-i] <= bufor[30-i];
			
		end	
end		
if(bufor[21] == 1'b0 & bufor[20] == 1'b0 & bufor[11] == 1'b1)begin				
for(int i=0 ; i<= 7; i++)
		begin			
			A_sin[7-i] <= bufor[19-i];
			
	    end	
end
//CALC OP
if(bufor[10] == 1'b0 & bufor[9] == 1'b1 & bufor[0] == 1'b1 &  bufor[8] == 1'b0)begin
for(int i=0 ; i<= 2; i++)
		begin			
			op_sin[2-i] <= bufor[7-i];
			
		end

//CALC CRC


for(int i=0 ; i<= 3; i++)
		begin			
			crc_sin[3-i] <= bufor[4-i];
			
		end
end		





case (op_sin)
or_op_sin : predicted_result= A_sin | B_sin;
and_op_sin: predicted_result= A_sin & B_sin;
add_op_sin: predicted_result= A_sin + B_sin;
sub_op_sin: predicted_result= A_sin - B_sin;
wrong_op_sin: predicted_result= 0;
wrong_op1_sin: predicted_result= 0;
wrong_op2_sin: predicted_result= 0;
wrong_op3_sin: predicted_result= 0;
endcase 


end
  end : read_sin
  
  
 /////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Powrwnywanie przewidywania z fajktycznym wyjsciem 
////////////////////////////////////////////////////////////
  
initial begin : scoreboard
  
  
  
  
end : scoreboard
  

endmodule : top
